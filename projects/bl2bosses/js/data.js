var data = [
	{
		"name" : "Vermivorous",
		"location": "Caustic Caverns, Tundra Express",
		"title": "The Invincible",
		"image": "img/Vermivorous.png",
		"biome" : "caustic",
		"drops": [
			{
				"weapon": "Norfleet",
				"class": "legendary",
				"description": "The Norfleet hurls three large purple balls of energy in a horizontal line. Each ball travels at a rather slow speed on an erratic trajectory (not in a straight line), with each ball dealing a tremendous amount of damage while covering a large area of effect. After an enemy takes damage from the Norfleet's projectiles, its body is surrounded by a blue ball of plasma, similarly to certain other E-tech launchers. ",
				"special": "Blows Up Everything!!!",
				"wiki" : "http://borderlands.wikia.com/wiki/Norfleet"
			},
			{
				"weapon" : "Nasty Surprise",
				"class" : "legendary",
				"special": "Supplies!",
				"description" : "Greatly reduced damage and blast radius. Longbow grenade which turns invisible during teleport. It drops one child grenade above each enemy up to a maximum of four. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Nasty_Surprise"
			},
            {
				"weapon": "Shredifier",
				"class": "legendary",
				"description": "Greatly decreased spool time, extremely high firing speed and very large capacity. Slightly reduced damage.",
				"special": "Speed Kills!",
				"wiki" : "http://borderlands.wikia.com/wiki/Shredifier"
			}
		]
	},
	{
		"name": "Spycho",
		"location": "Frostburn Canyon",
		"title": "",
		"image": "img/Spycho.png",
		"biome": "ice",
		"drops":
		[
			{
				"weapon": "Neogenator",
				"class": "legendary",
				"description": "Increased health and health regeneration. High elemental resistance.",
				"special": "For an impenetrable shield stand inside yourself",
				"wiki": "http://borderlands.wikia.com/wiki/Neogenator"
			}
		]
	},
	{
		"name" : "Wilhelm",
		"title": "",
		"location": "End of the Line",
		"image": "img/Wilhelm.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Logan's Gun",
				"class": "legendary",
				"description": "Always incendiary. Rounds explode once on contact, then continue traveling if they have not struck an inert surface (wall, ground, etc). Once they strike an inert surface there is a small delay, followed by a secondary explosion. ",
				"special": "Gun, Gunner!",
				"wiki" : "http://borderlands.wikia.com/wiki/Logan%27s_Gun"
			},
			{
				"weapon" : "Rolling Thunder",
				"class" : "legendary",
				"description" : "When thrown the grenade has very little arc and stays low to the ground. It behaves like a lobbed rubberized grenade at first but each bounce has a detonation. After several bounces or contact with any hostiles it explodes like a MIRV grenade. ",
				"special": "The thunder shall bring forth the pain! ",
				"wiki" : "http://borderlands.wikia.com/wiki/Rolling_Thunder"
			}
		]
	},
	{
		"name" : "Laney White",
		"title": "",
		"location": "The Fridge",
		"image": "img/LaneyWhite.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Gub",
				"class": "legendary",
				"description": "Massive magazine, heavily increased damage (around 10,000 at max level). Always Corrosive.",
				"special": "Abt Natural",
				"wiki" : "http://borderlands.wikia.com/wiki/Gub"
			}
		]
	},
	{
		"name" : "Savage Lee",
		"title": "",
		"location": "Three Horns",
		"image": "img/SavageLee.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Unkempt Harold",
				"class": "legendary",
				"description": "Fires 7 shots in an accelerating horizontal spread at the cost of 3 ammo per shot.",
				"special": "Did I fire six shots, or only five? Three? Seven. Whatever.",
				"wiki" : "http://borderlands.wikia.com/wiki/Unkempt_Harold"
			}
		]
	},
	{
		"name" : "Mad Dog",
		"title": "",
		"location": "Lynchwood",
		"image": "img/MadDog.png",
		"biome" : "desert ",
		"drops": 
		[
			{
				"weapon": "Madhous!",
				"class": "legendary",
				"description": "Bullets fired will follow a slightly swerving path that swerves back and forth left-right or up-down (orientation is random), then will split into two upon impact and bounce, bullets become faster after every bounce.",
				"special": "It's a Madhouse! A MADHOUSE!!!",
				"wiki" : "http://borderlands.wikia.com/wiki/Madhous"
			}
		]
	},
	{
		"name" : "McNally",
		"title": "",
		"location": "The Dust",
		"image": "img/McNally.png",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Hammer Buster",
				"class": "legendary",
				"description": "Unusually high bullet damage compared to similar rifles.",
				"special": "Gar! Gorarr! My dad's a scientist! GWARRRR!!!!",
				"wiki" : "http://borderlands.wikia.com/wiki/Hammer_Buster"
			}
		]
	},
	{
		"name" : "Pimon",
		"title": "",
		"location": "Wildlife Exploitation Preserve",
		"image": "img/Pimon.png",
		"biome" : "plains",
		"drops": 
		[
			{
				"weapon": "The Transformer",
				"class": "legendary",
				"description": "Immunity to shock damage. High chance to absorb enemy bullets. Electricity damage recharges the shield.",
				"special": "There's more than your eye can see.",
				"wiki" : "http://borderlands.wikia.com/wiki/The_Transformer"
			}
		]
	},
	{
		"name" : "BNK-3R",
		"title": "00101010",
		"location": "The Bunker",
		"image": "img/BNK3R.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Bitch",
				"class": "legendary",
				"description": " Critical hit damage bonus, achieves highest (to which it is near pinpoint) accuracy quicker than most other Hyperion Submachine Guns. ",
				"special": "yup. back.",
				"wiki" : "http://borderlands.wikia.com/wiki/Bitch_%28Borderlands_2%29"
			},
			{
				"weapon" : "The Sham",
				"class" : "legendary",
				"description" : "Extremely high bullet absorption chance. Greatly reduced shield capacity, reduced recharge delay and recharge rate. Cannot spawn with elemental resistance. ",
				"special" : "Wow. I CAN do this all day.",	
				"wiki" : "http://borderlands.wikia.com/wiki/Sham"
			}
		]
	},
	{
		"name" : "The Warrior",
		"title": "",
		"location": "Vault of the Warrior",
		"image": "img/Warrior.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Conference Call",
				"class": "legendary",
				"description": " Generates additional projectiles upon ricochet or after sufficient distance. Reduced accuracy and recoil recovery. ",
				"special": "Let's just ping everyone all at once.",
				"wiki" : "http://borderlands.wikia.com/wiki/Conference_Call"
			},
			{
				"weapon" : "The Impaler",
				"class" : "legendary",
				"description" : " Launches Corrosive homing spikes when damaged at range. Deals Corrosive spike damage to melee attackers. ",
				"special": "Vlad would be proud.",
				"wiki" : "http://borderlands.wikia.com/wiki/Impaler"
			},
			{
				"weapon" : "The Flakker",
				"class" : "legendary",
				"description" : "Increased damage, Consumes 4 ammo per shot, extremely large spread, rounds detonate after reaching a certain distance. ",
				"special": "Flak The World",
				"wiki" : "http://borderlands.wikia.com/wiki/Flakker"
			},
			{
				"weapon" : "Leech",
				"class" : "legendary",
				"description" : "Always elemental. Decreased radius and damage. Spawns homing child grenades. All damage done by the grenade and child grenades instantly restores allies' health. ",
				"special": "A skilful leech is better far, than half a hundred men of war. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Leech"
			},
			{
				"weapon" : "Volcano",
				"class" : "legendary",
				"description" : "Large bonus to incendiary effect. Emits a fire cloud upon impact, spreading incendiary damage over nearby enemies. ",
				"special": "Pele humbly requests a sacrifice, if it's not too much trouble.",
				"wiki" : "http://borderlands.wikia.com/wiki/Volcano_%28Borderlands_2%29"
			}
		]
	},
	{
		"name" : "Pyro Pete",
		"title": "The Invincible",
		"image": "img/PyroPete.png",
		"location": "Pyro Pete's Bar",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "EVERY SINGLE ONE",
				"class": "legendary",
				"description": "GO FARM THAT MOTHERFUCKER",
				"wiki" : "http://borderlands.wikia.com/wiki/Legendary"
			}
		]
	},
	{
		"name" : "Hyperius",
		"title": "The Invincible",
		"image": "img/Hyperius.png",
		"location": "Washburne Refinery",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "EVERY SINGLE ONE",
				"class": "legendary",
				"description": "GO FARM THAT MOTHERFUCKER",
				"wiki" : "http://borderlands.wikia.com/wiki/Legendary"
			}
		]
	},
	{
		"name" : "Badassausorus Rex",
		"title": "",
		"image": "img/Badassausorus.png",
		"location": "Torgue Arena",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Slow Hand",
				"class": "etech",
				"description": " Fires a slow-moving projectile unaffected by gravity with a high elemental effect chance and splash damage. Inflicted damage restores health to the weapon's wielder. ",
				"special": "Take your time, Sugar...",
				"wiki" : "http://borderlands.wikia.com/wiki/Slow_Hand"
			}
		]
	},
	{
		"name" : "Jackenstein",
		"title": "",
		"image": "img/Jackenstein.png",
		"location": "HSS Terminus",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Yellow Jacket",
				"class": "etech",
				"description": " Always Shock elemental, increased damage, enhanced magazine size, greatly increased projectile size. Projectiles gradually pick up speed, only to become invisible after a certain distance. While invisible, projectiles continue to accelerate until hitting an enemy. Projectiles bounce off the first surface they come in contact with and have a small AoE blast radius.  ",
				"special": "By the people. For the people.",
				"wiki" : "http://borderlands.wikia.com/wiki/Yellow_Jacket"
			}
		]
	},
	{
		"name" : "Boom & Bewm",
		"title": "",
		"image": "img/BoomBewm.png",
		"location": "Southern Shelf",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Bonus Package",
				"class": "legendary",
				"description": " Increased damage. When child grenades explode, each releases one additional child grenade. ",
				"special": "2x more awesome, bonus extreme!",
				"wiki" : "http://borderlands.wikia.com/wiki/Bonus_Package"
			}
		]
	},
	{
		"name" : "Captain Flynt",
		"title": "",
		"image": "img/Flynt.png",
		"location": "The Soaring Dragon",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Thunderball Fists",
				"class": "legendary",
				"special": "I can have such a thing?",
				"description": "Shots fired produce electrical orbs which upon impact deal an AoE burst of electric damage before flaring upwards for a few seconds then detonating a second time with shocking results.",
				"wiki" : "http://borderlands.wikia.com/wiki/Thunderball_Fists"
			},
            {
                "weapon": "Flynt's Tinderbox",
                "class": "blue",
                "special": "Good for starting fires",
                "description": "Always fire elemental. High elemental effect chance. Projectiles travel in a low parabolic arc. ",
                "wiki": "http://borderlands.wikia.com/wiki/Flynt%27s_Tinderbox"
            }
		]
	},
    {
        "name": "Big Sleep",
        "title": "",
        "location": "Hayter's Folly",
        "image": "",
        "biome": "haytersfolly",
        "drops" :
        [
            {
                "weapon": "12 Pounder",
                "class": "blue",
                "special": "Nec Pluribus Impar, Bitches",
                "description": "Increased damage, reduced magazine size, and fast reload speed. Fires arcing cannonballs that explode if they directly hit an enemy or bounce with parabolic trajectory if they hit an inert surface before exploding. Cannonballs are capable of scoring Critical hits.",
                "wiki": "http://borderlands.wikia.com/wiki/12_Pounder"
            }
        ]
    },
    {
        "name": "No-Beard",
        "title": "",
        "location": "Oasis",
        "image": "",
        "biome": "oasis",
        "drops" :
        [
            {
                "weapon": "Stinkpot",
                "class": "blue",
                "special": "We're pirates, we don't follow the rules",
                "description": "Large Corrosive splash effect. Contagious corrosive damage spreads to other enemies. Rounds that strike surfaces at an angle ricochet. ",
                "wiki": "http://borderlands.wikia.com/wiki/12_Pounder"
            }
        ]
    },
    {
        "name": "Mad Moxxi",
        "title": "(No, you can't kill her)",
        "location": "Sanctuary",
        "image": "",
        "biome": "sanctuary",
        "drops" :
        [
            {
                "weapon": "Mad Moxxi's Bad Touch",
                "class": "purple",
                "special": "When I'm good, I'm very good...",
                "description": "Always Corrosive. +70% Critical Hit damage. While wielding this gun, dealing damage (from any damage, either the gun itself or special abilities or even grenades) causes health regeneration (2% of damage inflicted). ",
                "wiki": "http://borderlands.wikia.com/wiki/Bad_Touch"
            },
            {
                "weapon": "Mad Moxxi's Good Touch",
                "class": "purple",
                "special": "...but when I'm bad, I'm better.",
                "description": "Always incendiary. +70% Critical Hit damage. Dealing any kind of damage dealt while wielding this weapon regenerates health (2.5% of damage inflicted). ",
                "wiki": "http://borderlands.wikia.com/wiki/Good_Touch"
            }
        ]
    },
	{
		"name" : "Knuckledragger",
		"title": "",
		"location": "Windshear Waste",
		"image": "img/Knuckle.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Hornet",
				"class": "legendary",
				"special" :"Fear The Swarm",
				"description": "Always corrosive. 5-round burst fire with high Elemental Effect. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Hornet_%28Borderlands_2%29"
			}
		]
	},
	{
		"name" : "Ultimate Badass Varkid",
		"title": "",
		"location": "Everywhere",
		"image": "img/UBVarkid.png",
		"biome" : "caustic",
		"drops": 
		[
			{
				"weapon": "Quasar",
				"class": "legendary",
				"special" :"E=mc^(OMG)/wtf",
				"description": "Always Shock Singularity Grenade. Reduced damage but increased blast radius (larger singularity pull and Tesla effect). Enemies pulled within blast radius suffer shock damage over time until explosion occurs.",
				"wiki" : "http://borderlands.wikia.com/wiki/Quasar"
			}
		]
	},
	{
		"name" : "Chubbies",
		"title": "",
		"location": "Everywhere",
		"image": "img/Chubbies.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Whisky Tango Foxtrot",
				"class": "legendary",
				"special" :"Situation normal..., ...all foxtroted up",
				"description": "Chance to drop an IED Booster when damaged. Spawns 3 volleys of shock boosters.",
				"wiki" : "http://borderlands.wikia.com/wiki/Hornet_%28Borderlands_2%29"
			},
			{
				"weapon": "Bunny",
				"class": "legendary",
				"special" :"Hippity Hoppity!",
				"description": "When reloaded it drops out active grenades at random while bouncing like a rabbit leaping.",
				"wiki" : "http://borderlands.wikia.com/wiki/Bunny"
			}
		]
	},
	{
		"name" : "The Black Queen",
		"title": "",
		"image": "img/BlackQueen.png",
		"location": "The Dust",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Nukem",
				"class": "legendary",
				"description": "Rockets fire in an arc and explode in a mushroom cloud upon detonation. Extremely slow reload speed. ",
				"special": "Name Dropper.",
				"wiki" : "http://borderlands.wikia.com/wiki/Nukem"
			}
		]
	},
	{
		"name" : "Doc Mercy",
		"title": "",
		"image": "img/DocMercy.png",
		"location": "Three Horns - Valley",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Infinity",
				"class": "legendary",
				"description": "Does not consume ammunition. Shots fired form a fixed lemniscate (∞) pattern, ignoring normal recoil behavior. Increased accuracy when aiming down the sights. ",
				"special": "It's closer than you think! (no it isn't)",
				"wiki" : "http://borderlands.wikia.com/wiki/Infinity"
			}
		]
	},
	{
		"name" : "Gettle",
		"title": "",
		"image": "img/Gettle.png",
		"location": "The Dust",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Lyuda",
				"class": "legendary",
				"description": " Shoots one projectile that splits into three projectiles after a certain distance on a horizontal plane. Distance at which the bullet splits is relevant to distance to target, up until a maximum distance. Each shot comes at the cost of one sniper bullet. ",
				"special": "Man Killer",
				"wiki" : "http://borderlands.wikia.com/wiki/Lyuda"
			}
		]
	},
	{
		"name" : "Dukino's Mom",
		"title": "",
		"image": "img/Dukino.png",
		"location": "Lynchwood",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Mongol",
				"class": "legendary",
				"description": "Fires a single large rocket that sporadically releases multiple smaller rockets at random angles along its flightpath.",
				"special": "The horde will always return! ",
				"wiki" : "http://borderlands.wikia.com/wiki/Mongol"
			}
		]
	},
	{
		"name" : "Henry",
		"title": "",
		"image": "img/Henry.png",
		"location": "The Highlands",
		"biome" : "plains",
		"drops": 
		[
			{
				"weapon": "The Cradle",
				"class": "legendary",
				"description": " When shield is depleted it is disposed of...with explosive results. ",
				"special": "...to the grave.",
				"wiki" : "http://borderlands.wikia.com/wiki/The_Cradle"
			}
		]
	},
	{
		"name" : "Tumbaa",
		"title": "",
		"image": "img/Tumbaa.png",
		"location": "Wildlife Exploitation Preserve",
		"biome" : "plains",
		"drops": 
		[
			{
				"weapon": "Deliverance",
				"class": "legendary",
				"description": "Reload-thrown weapon continues to fire after thrown and homes in on targets.",
				"special": "Kiki got a shotgun",
				"wiki" : "http://borderlands.wikia.com/wiki/Deliverance"
			}
		]
	},
	{
		"name" : "Hunter Hellquist",
		"title": "",
		"image": "img/HHQ.png",
		"location": "Arid Nexus - Boneyard",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "The Bee",
				"class": "legendary",
				"description": "Large amount of Amp shot damage, 0 Amp Shot Drain. Lowered shield capacity. Increased Shield Recharge Rate. Increased Recharge Delay. ",
				"special": "Float like a butterfly...",
				"wiki" : "http://borderlands.wikia.com/wiki/The_Bee"
			}
		]
	},
	{
		"name" : "King Mong",
		"location" : "Eridium Blight",
		"title": "",
		"image": "img/KingMong.png",
		"biome" : "eridium",
		"drops": 
		[
			{
				"weapon": "Badaboom",
				"class": "legendary",
				"description": " Fires a cluster of six rockets at the cost of one rocket ammo, each rocket with slightly reduced damage.",
				"special": "Multi-kill.",
				"wiki" : "http://borderlands.wikia.com/wiki/Badaboom"
			}
		]
	},
	{
		"name" : "Mick Zaford",
		"title": "",
		"image": "img/Zaford.png",
		"location": "The Dust",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Maggie",
				"class": "legendary",
				"description": "Shoots 6 projectiles at the price of 1 bullet. acting much like a shotgun, but with higher accuracy. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Maggie"
			}
		]
	},
	{
		"name" : "Midge Mong",
		"title": "",
		"image": "img/MidgeMong.png",
		"location": "Southern Shelf - Bay",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "KerBlaster",
				"class": "legendary",
				"description": " Consumes 4 ammo per shot. Each shot fires one projectile which has a straight trajectory and explodes on impact. The explosion releases a grenade which also explodes one second later. This weapon cannot score critical hits. ",
				"special": "Torgue got more BOOM!",
				"wiki" : "http://borderlands.wikia.com/wiki/KerBlaster"
			}
		]
	},
	{
		"name" : "Mobley",
		"title": "",
		"location": "The Dust",
		"image": "img/Mobley.png",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Veruc",
				"class": "legendary",
				"description": "Fires 3 projectiles at the cost of 2 bullets in a horizontally linear spread. When zoomed, fires a five-round burst of the same pattern, but each group gets progressively tighter towards the center bullet - the first group of the burst is the same as if hip-fired; the fifth is very close to centered. ",
				"special": "I want that rifle, Daddy!",
				"wiki" : "http://borderlands.wikia.com/wiki/Veruc"
			}
		]
	},
	{
		"name" : "Loot Chests",
		"title": "",
		"location": "Everywhere",
		"image": "img/Chest.png",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Fire Bee",
				"class": "legendary",
				"description": "Always incendiary. Spits fire in a circular motion. Additionally shoots small missiles of fire in a circular motion similar to the Hive rocket launcher.",
				"special": "Bees are coming!",
				"wiki" : "http://borderlands.wikia.com/wiki/Fire_Bee"
			},
			{
				"weapon": "Every single one",
				"class" : "legendary",
				"description": "Like, every weapon. Surprising uh ?",
				"wiki": "http://borderlands.wikia.com/wiki/Legendary"
			}
		]
	},
	{
		"name" : "Loot Midget",
		"title": "",
		"location": "Loot Chests",
		"image": "img/LootMidget.png",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Bouncing Bonny",
				"class": "legendary",
				"description": "Increased damage, blast radius, and reduced fuse time. Bouncing Betty-type grenade also disperses child grenades with every bounce.",
				"special": "Your sister is such a bitch.",
				"wiki" : "http://borderlands.wikia.com/wiki/Veruc"
			}
		]
	},
	{
		"name" : "Mortar",
		"title": "",
		"image": "img/Mortar.png",
		"location": "Sawtooth Cauldron",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Pandemic",
				"class": "legendary",
				"description": "Always corrosive. After the grenade's initial explosion, three more homing grenades are spawned. The grenades spawned will home in on enemies and touch them, transferring corrosive damage and then seek out another target. This will continue until the grenades disappear. ",
				"special": "Spread the sickness.",
				"wiki" : "http://borderlands.wikia.com/wiki/Pandemic"
			}
		]
	},
	{
		"name" : "Lee, Mike, Dan & Ralph",
		"title": "",
		"image": "img/Turtles.png",
		"location": "Bloodshot Stronghold",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Storm Front",
				"class": "legendary",
				"description": "Deploys as a Tesla Grenade, then launches multiple smaller range, lower damage child Tesla grenades in an even distribution around the deployment point. Child grenades can stick to walls and other non-horizontal surfaces.",
				"special": "Sometimes lightning does strike twice",
				"wiki" : "http://borderlands.wikia.com/wiki/Storm_Front"
			}
		]
	},
	{
		"name" : "Old Slappy",
		"title": "",
		"location": "The Highlands - Outwash",
		"image": "img/Slappy.png",
		"biome" : "plains",
		"drops": 
		[
			{
				"weapon": "Striker",
				"class": "legendary",
				"description": "Additional critical damage, added with exceptional accuracy and minuscule spread. Increased weapon zoom when sighted. ",
				"special": "Fandir? Thirteen",
				"wiki" : "http://borderlands.wikia.com/wiki/Striker_%28Borderlands_2%29"
			}
		]
	},
	{
		"name" : "Rakkmann",
		"title": "",
		"image": "img/Rakkmann.png",
		"location": "The Fridge",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Gunerang",
				"class": "legendary",
				"description": "When reloaded it is thrown and travels in a straight line for a medium distance. It then turns around and homes in on the player. If it comes in close proximity to an enemy it will home in and explode on the enemy, otherwise it continues to home in on the player until it explodes. ",
				"special": "Give it a burl.",
				"wiki" : "http://borderlands.wikia.com/wiki/Gunerang"
			}
		]
	},
	{
		"name" : "Saturn",
		"title": "",
		"image": "img/Saturn.png",
		"location": "Arid Nexus - Badlands",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Invader",
				"class": "legendary",
				"description": "Fires a burst of 5 rounds when scoped.",
				"special": "The Executioner has arrived.",
				"wiki" : "http://borderlands.wikia.com/wiki/Invader_(Borderlands_2)"
			},
            {
				"weapon": "Hive",
				"class": "blue",
				"description": " After firing, the hive rocket will stick to the first object it hits; if it doesn't hit anything, it will travel a certain distance before stopping in mid-air. It will then begin to spin and release small homing corrosive rockets at a rate of about 2 rockets per second for approximately 7 seconds. After this effect ends the main rocket will explode.",
				"special": "full of bees.",
				"wiki" : "http://borderlands.wikia.com/wiki/Hive"
			}
            
		]
	},
    {
		"name" : "Bone-Head 2.0",
		"title": "",
		"image": "",
		"location": "Arid Nexus - Badlands",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Bone Shredder",
				"class": "purple",
				"description": "Mandatory double accessory, lower accuracy than most average Bandit SMGs of its level. ",
				"special": "The Lead Wind Blows!",
				"wiki" : "http://borderlands.wikia.com/wiki/Bone_Shredder_%28Borderlands_2%29"
			}
		]
	},
    {
		"name" : "Rouge",
		"title": "",
		"image": "",
		"location": "Candlerakk's Crag",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Hydra",
				"class": "purple",
				"description": "Very high pellet count. Spread forms five groupings in a horizontal line. ",
				"special": "5 heads of Death!",
				"wiki" : "http://borderlands.wikia.com/wiki/Hydra_%28Borderlands_2%29"
			}
		]
	},
	{
		"name" : "Badass Creeper",
		"title": "",
		"location": "Caustic Caverns",
		"image": "img/Creeper.png",
		"biome" : "caustic",
		"drops": 
		[
			{
				"weapon": "Longbow",
				"class": "legendary",
				"description": "It fires slow moving projectiles shaped like arrows. Always incendiary. Always spawns without a scope. Even though it is an E-tech weapon, it only consumes one sniper bullet per shot.",
				"special": "Ceci n'est pas une sniper rifle!",
				"wiki" : "http://borderlands.wikia.com/wiki/Invader_(Borderlands_2)"
			},
            {
				"weapon": "Blockhead",
				"class": "blue",
				"description": "It fires a unique 3x3 spread of fireball blocks that ricochet off of surfaces. These pellets ricochet once, and explode upon contact with an enemy. ",
				"special": "Also try Minecraft!",
				"wiki" : "http://borderlands.wikia.com/wiki/Blockhead"
			}
		]
	},
	{
		"name" : "Incinerator Clayton",
		"title": "",
		"location": "Frostburn Canyon",
		"image": "img/Clayton.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Pyrophobia",
				"class": "legendary",
				"description": "Always Incendiary. Projectiles explode multiple times mid-flight before hitting their target.",
				"special": "It's actually a fairly rational fear in this case.",
				"wiki" : "http://borderlands.wikia.com/wiki/Pyrophobia"
			}
		]
	},
	{
		"name" : "Foreman Jasper",
		"title": "",
		"location": "Opportunity",
		"image": "img/Jasper.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Black Hole",
				"class": "legendary",
				"description": "Pulls nearby enemies towards the wearer when its shield charge is depleted, then explodes in a huge nova. Increased damage and range, always shock element.",
				"special": "You are the center of the universe.",
				"wiki" : "http://borderlands.wikia.com/wiki/Black_Hole"
			}
		]
	},
	{
		"name" : "Scorch",
		"title": "",
		"image": "img/Scorch.png",
		"location": "Frostburn Canyon",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Hellfire",
				"class": "legendary",
				"description": "High fire elemental damage, high elemental effect chance, small explosion damage radius which deals reduced damage and multiple proc stacking. ",
				"special": "We don't need no fire...",
				"wiki" : "http://borderlands.wikia.com/wiki/Hellfire_%28Borderlands_2%29"
			}
		]
	},
	{
		"name" : "Boll",
		"title": "",
		"location": "Three Horns - Divide",
		"image": "img/Boll.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Fastball",
				"class": "legendary",
				"description": " Increased base damage, and travel speed. Minimal fuse time and almost null blast radius.",
				"special": "Forget the curveball Ricky, give him the heater.",
				"wiki" : "http://borderlands.wikia.com/wiki/Fastball"
			}
		]
	},
	{
		"name" : "Blue",
		"title": "",
		"location": "Caustic Caverns",
		"image": "img/Blue.png",
		"biome" : "caustic",
		"drops": 
		[
			{
				"weapon": "Fabled Tortoise",
				"class": "legendary",
				"description": "Very high shield capacity, but reduced movement speed while active. Movement speed is increased beyond normal upon shield depletion. Greatly reduced health.",
				"special": "Win by a hare",
				"wiki" : "http://borderlands.wikia.com/wiki/Fabled_Tortoise"
			}
		]
	},
    {
		"name" : "Peter Zaford",
		"title": "",
		"location": "The Holy Spirits",
		"image": "",
		"biome" : "caustic",
		"drops": 
		[
			{
				"weapon": "Pot O' Gold",
				"class": "blue",
				"description": "When damaged the shield has a small chance to drop money as well as shield boosters.  ",
				"special": "Blarney I say! 'Tis blarney!",
				"wiki" : "http://borderlands.wikia.com/wiki/Pot_O%27_Gold"
			}
		]
	},
    {
		"name" : "Bulstoss",
		"title": "",
		"location": "Hunter's Grotto",
		"image": "",
		"biome" : "caustic",
		"drops": 
		[
			{
				"weapon": "Rex",
				"class": "blue",
				"description": "Increased damage and critical hit damage. Very low fire rate for a Jakobs pistol. ",
				"special": "Basically, it's a big gun.",
				"wiki" : "http://borderlands.wikia.com/wiki/Rex"
			}
		]
	},
    {
		"name" : "Bulwark",
		"title": "",
		"location": "Hunter's Grotto",
		"image": "",
		"biome" : "caustic",
		"drops": 
		[
			{
				"weapon": "The Rough rider",
				"class": "blue",
				"description": " 0 shield capacity. Increases resistance to all damage types. Increases maximum health. ",
				"special": "It takes more than that to kill a Bull Moose.",
				"wiki" : "http://borderlands.wikia.com/wiki/The_Rough_Rider"
			}
		]
	},
    {
		"name" : "Lil' Sis",
		"title": "",
		"location": "Magnys Lighthouse",
		"image": "",
		"biome" : "caustic",
		"drops": 
		[
			{
				"weapon": "Little Evie",
                "class": "blue",
				"description": "Obtained from Lil' Sis after killing Mr. Bubbles without hurting her in the process. Always shock elemental. Very high elemental chance and shock damage. Also has very high fire rate for a Maliwan pistol. Projectiles fire in a parabolic arc. Killing an enemy increases action skill cooldown rate by 12% ",
				"special": "Shock and awwwww! So cute!",
				"wiki" : "http://borderlands.wikia.com/wiki/Little_Evie"
			}
		]
	},
	{
		"name" : "Sheriff",
		"title": "",
		"image": "img/Sheriff.png",
		"location": "Lynchwood",
		"biome" : "plains",
		"drops": 
		[
			{
				"weapon": "Sheriff's Badge",
				"class": "purple",
				"description": "Increases pistol fire rate, pistol damage, and Fight For Your Life bleed-out time. Also further increases Fight For Your Life time and Max Health by 15% each for every equipped Deputy's Badge in the party. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Sheriff%27s_Badge"
			},
            {
				"weapon": "Law",
				"class": "blue",
				"description": "Always spawns with a blade, granting a +100% melee damage bonus. In addition, if shield 'Order' is equipped, melee attacks with Law will restore the character's health. 100% accuracy when scoped (not shown on weapon card). ",
				"wiki" : "http://borderlands.wikia.com/wiki/Law_%28Borderlands_2%29"
			}
		]
	},
	{
		"name" : "Smash-Head",
		"title": "",
		"location": "The Fridge",
		"image": "img/SmashHead.png",
		"biome" : "ice",
		"drops": 
		[
			{
				"weapon": "Sledge's Shotgun",
				"class": "legendary",
				"description": "Burst-fires two shots per trigger pull, with a long delay between each burst. Reduced clip size ",
				"special" : "The Legend Lives",
				"wiki" : "http://borderlands.wikia.com/wiki/Sledge%27s_Shotgun_%28Borderlands_2%29"
			}
		]
	},
	{
		"name" : "Son of Mothrakk",
		"title": "",
		"location": "Wildlife Exploitation Preserve",
		"image": "img/Mothrakk.png",
		"biome" : "plains",
		"drops": 
		[
			{
				"weapon": "Skullmasher",
				"class": "legendary",
				"description": "Shoots five projectiles at the cost of one round. ",
				"special": "Makes your brain hurt.",
				"wiki" : "http://borderlands.wikia.com/wiki/Skullmasher_%28Borderlands_2%29"
			}
		]
	},
	{
		"name" : "Tector and Jimbo Hodunk",
		"title": "",
		"location" : "The Dust",
		"image": "img/Tector.png",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Slagga",
				"class": "legendary",
				"description": "Always Slag element. Fires 3 slag projectiles with reduced damage per shot at a cost of one SMG ammo. High chance to slag. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Slagga",
				"special": "blagaga"
			}
		]
	},
    {
		"name" : "Bloodtail",
		"title": "",
		"location" : "Candlerakk's Crag",
		"image": "",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Damned Cowboy",
				"class": "blue",
				"description": "Sports an extremely high damage output and accuracy at the sacrifice of fire rate. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Damned_Cowboy",
				"special": "Speak Softly. Carry This."
			}
		]
	},
    {
		"name" : "Arizona",
		"title": "",
		"location" : "Candlerakk's Crag",
		"image": "",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Elephant Gun",
				"class": "blue",
				"description": "Extremely high damage output. Always spawns without a scope. Very low fire rate. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Elephant_Gun",
				"special": "Hated by large numbers of people."
			}
		]
	},
    {
		"name" : "Omnd-Omnd-Ohk",
		"title": "",
		"location" : "Hunter's Grotto, Candlerakk's Crag, Scylla's Grove",
		"image": "",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Twister",
				"class": "blue",
				"description": "Fires a cluster of spherical projectiles that quickly transform into a tornado over a short distance. Always shock.",
				"wiki" : "http://borderlands.wikia.com/wiki/Twister",
				"special": "Putting trouble on the run."
			}
		]
	},
	{
		"name" : "Assassins Wot, Oney, Reeth and Rouf",
		"title": "",
		"location": "Southpaw Steam & Power",
		"image": "img/Assassins.png",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Emperor",
				"class": "legendary",
				"description": "Delivers a burst of 6 bullets to the target at reliable recoil. Very fast accuracy recovery.",
				"wiki" : "http://borderlands.wikia.com/wiki/Emperor",
				"special": "you know... for him."
			},
            {
				"weapon": "Commerce",
				"class": "blue",
				"description": "Always Shock, reduced damage and faster reload. When moving, a flashing animated effect covers the entire weapon surface. Also gains accuracy faster than most Hyperion firearms, with the exception of the Bitch. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Commerce",
				"special": "I have a Soldier, a Siren, two Scooters and a Claptrap."
			},
            {
				"weapon": "Dog",
				"class": "blue",
				"description": "Increased damage, fire rate and magazine size at the cost of stability.",
				"wiki" : "http://borderlands.wikia.com/wiki/Dog",
				"special": "Because one barrel ain't enough, and two is too few."
			},
            {
				"weapon": "Fremington's Edge",
				"class": "blue",
				"description": "Increased zoom, increased critical hit damage. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Fremington%27s_Edge",
				"special": "I can see my house from here."
			},
            {
				"weapon": "Judge",
				"class": "blue",
				"description": " Bonus critical hit damage. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Judge",
				"special": "I am free now."
			}
		]
	},
	{
		"name" : "Madame Von Bartlesby",
		"title": "",
		"image": "img/Bartlesby.png",
		"location": "Tundra Express",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Baby Maker",
				"class": "legendary",
				"description": " Thrown weapon spawns a 'child grenade' upon exploding, an identical but smaller version of the original gun that explodes again. Very rarely it will spawn a second or third projectile. ",
				"special": "Who's a widdle gunny wunny!!!",
				"wiki" : "http://borderlands.wikia.com/wiki/Baby_Maker"
			},
			{
				"weapon": "Madhous!",
				"class": "legendary",
				"description": "Bullets fired will follow a slightly swerving path that swerves back and forth left-right or up-down (orientation is random), then will split into two upon impact and bounce, bullets become faster after every bounce.",
				"special": "It's a Madhouse! A MADHOUSE!!!",
				"wiki" : "http://borderlands.wikia.com/wiki/Madhous"
			}
		]
	},
	{
		"name" : "Terramorphous",
		"title": "The Invincible",
		"image": "img/Terramorphous.png",
		"location": "Terramorphous Peak",
		"biome" : "plains",
		"drops": 
		[
			{
				"weapon": "Slayer of Terramorphous",
				"class": "legendary",
				"description": " ",
                "special": "",
				"wiki" : "http://borderlands.wikia.com/wiki/Slayer_of_Terramorphous"
			},

			{
				"weapon": "Hide of Terramorphous",
				"class": "legendary",
				"description": " It is a combination of the Maylay, Nova (fire), and Spike (fire) shield types. It also gives an extremely high roid damage boost. ",
				"special" : "...His hide turned the mightiest tame...",
				"wiki" : "http://borderlands.wikia.com/wiki/Hide_of_Terramorphous"
			},

			{
				"weapon": "Blood of Terramorphous",
				"class": "legendary",
				"description": "Adds health regeneration. ",
				"special": "...His blood could inspire...",
				"wiki" : "http://borderlands.wikia.com/wiki/Blood_of_Terramorphous"
			},

			{
				"weapon": "Breath of Terramorphous",
				"class": "legendary",
				"description": " Thrown weapon spawns a 'child grenade' upon exploding, an identical but smaller version of the original gun that explodes again. Very rarely it will spawn a second or third projectile. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Breath_of_Terramorphous"
			},
			{
				"weapon": "Shredifier",
				"class": "legendary",
				"description": "Greatly decreased spool time, extremely high firing speed and very large capacity. Slightly reduced damage.",
				"special": "Speed Kills!",
				"wiki" : "http://borderlands.wikia.com/wiki/Shredifier"
			},

			{
				"weapon": "Pitchfork",
				"class": "legendary",
				"description": "Fires 5 horizontal shots in a line, shaped in a 'V' pattern. ",
				"special": "Mainstream'd!",
				"wiki" : "http://borderlands.wikia.com/wiki/Pitchfork"
			},
            {
				"weapon": "Teeth of Terramorphous",
				"class": "blue",
				"description": "Always incendiary. Fires two horizontal rows of 6 rounds each. The two rows rise then collapse into each other (much like a mouth opening and closing) after a specific distance. When the rows collapse they cause six small incendiary explosions and disappear. ",
				"special": "His teeth made them flee in shame...",
				"wiki" : "http://borderlands.wikia.com/wiki/Pitchfork"
			}
		]
	},
	{
		"name" : "Master Gee",
		"title": "The Invincible",
		"image": "img/MasterGee.png",
		"location": "Hayter's Folly",
		"biome" : "desert",
		"drops": 
		[
			{
				"weapon": "Every single one",
				"class": "legendary",
				"description": "What are you waiting for ?",
				"wiki" : "http://borderlands.wikia.com/wiki/Legendary",
                "special": "",
			}
		]
	},
	{
		"name" : "Voracidous",
		"title": "The Invincible",
		"image": "img/Voracidous.png",
		"location": "Candlerakk's Crag",
		"biome" : "plains",
		"drops": 
		[
			{
				"weapon": "Every class mod",
				"class": "legendary",
				"special": "",
                "description": "What are you waiting for ?",
				"wiki" : "http://borderlands.wikia.com/wiki/Legendary"
			}
		]
	},
        {
		"name" : "Pete's Burners",
		"title": "",
		"image": "",
		"location": "The Beatdown",
		"biome" : "plains",
		"drops": 
		[
			{
				"weapon": "Cobra",
				"class": "blue",
				"description": "Fires projectiles with Explosive properties. ",
				"wiki" : "http://borderlands.wikia.com/wiki/Cobra_%28Borderlands_2%29",
                "special": "Found out about this I was like DAAAMN, I'm bringing that gun BACK!"
			}
		]
	},
	{
		"name" : "Dexiduous",
		"title": "The Invincible",
		"image": "img/Dexiduous.png",
		"location": "Candlerakk's Crag",
		"biome" : "plains",
		"drops": 
		[
			{
				"weapon": "Every seraph",
				"class": "legendary",
				"special": "",
                "description": "What are you waiting for ?",
				"wiki" : "http://borderlands.wikia.com/wiki/Seraph"
			},
            {
                "weapon": "CHOPPER",
                "class": "blue",
                "special": "Get to it.",
                "description": "Massive magazine size. Continuously fire with only one press of the trigger. Extremely fast fire rate and low accuracy. Fires 4 projectiles at the cost of 6. Extended reload time.",
                "wiki": "http://borderlands.wikia.com/wiki/CHOPPER"
            }
		]
	}
]


var phrases = [
	"This boss looks terrible.",
	"Raaaaaaaaaaaaah.",
	"Give me some challenge."
]