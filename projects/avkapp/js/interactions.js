var interactions = [
	{
		"name": "Allopurinol",
		"inr": "up",
		"desc": "Jusqu'a 8 jours après l'arrêt"
	},
	{
		"name": "Amiodarone",
		"inr": "up",
		"desc": "Jusqu'à 8 jours après l’arrêt"
	},
	{
		"name": "Androgenes",
		"inr": "up",
		"desc": "Jusqu'a 8 jours après l'arrêt"
	},
	{
		"name": "ISRS : Citalopram, Escitalopram, Fluoxetine, Fluvoxamine, Paroxetine, Sertraline",
		"inr": "up",
		"desc": ""
	},
	{
		"name": "Benzbromaron",
		"inr": "up",
		"desc": ""
	},
	{
		"name": "Aprepitants",
		"inr": "down",
		"desc": ""
	},
	{
		"name": "Aminogluthetimides",
		"inr": "down",
		"desc": "Cancer du sein et de la prostate. Jusqu'a 2 semaines après l'arrêt"
	},
	{
		"name": "Azathioprine",
		"inr": "down",
		"desc": ""
	},
	{
		"name": "Céphalosporine : Cefamandole, Cefoperazone, Cefotetan",
		"inr": "up",
		"desc": ""
	},
	{
		"name": "Ceftriaxone",
		"inr": "up",
		"desc": ""
	},
	{
		"name": "Cimetidine",
		"inr": "up",
		"desc": "Jusqu'a 8 jours après l'arrêt"
	},
	{
		"name": "Cisapride",
		"inr": "up",
		"desc": "Jusqu'a 8 jours après l'arret"
	},
	{
		"name": "Colchicine",
		"inr": "up",
		"desc": "Jusqu'a 8 jours après l'arret"
	},
	{
		"name": "Cycline",
		"inr": "up",
		"desc": ""
	},
	{
		"name": "Danazol",
		"inr": "up",
		"desc": ""
	},
	{
		"name": "Econazole",
		"inr": "up",
		"desc": "Quelque soit le mode d'administration"
	},
	{
		"name": "Fibrate",
		"inr": "up",
		"desc": "Jusqu'a 8 jours après l'arrêt"
	},
	{
		"name": "Fluconazole, Itraconazole, Voriconazole",
		"inr": "up",
		"desc": "Jusqu'a 8 jours après l'arrêt"
	},
	{
		"name": "Oflaxacine, Pefloxacine, Enoxacine, Lomefloxacine, Moxifloxacine,Ciprofloxacine,Levofloxacine,Norfloxacine",
		"inr": "up",
		"desc": ""
	}

]
